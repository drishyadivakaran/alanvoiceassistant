package com.wr.alanvoiceassistant;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.alan.alansdk.AlanCallback;
import com.alan.alansdk.AlanConfig;
import com.alan.alansdk.button.AlanButton;
import com.alan.alansdk.events.EventCommand;
import com.google.android.material.tabs.TabLayout;
import com.wr.alanvoiceassistant.databinding.ActivityMain2Binding;
import com.wr.alanvoiceassistant.ui.main.SectionsPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity2 extends AppCompatActivity {

    private ActivityMain2Binding binding;
    AlanButton alanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
//        FloatingActionButton fab = binding.fab;
//
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        /// Defining the project key
        AlanConfig config = AlanConfig.builder().setProjectId("b5fd04cf79f3b2f6c443512913fe289a2e956eca572e1d8b807a3e2338fdd0dc/stage").build();

        alanButton.initWithConfig(config);
        AlanCallback alanCallback = new AlanCallback() {
            /// Handling commands from Alan Studio
            @Override
            public void onCommand(final EventCommand eventCommand) {
                try {
                    JSONObject command = eventCommand.getData();
                    String commandName = command.getJSONObject("data").getString("command");
                    Log.d("AlanButton", "onCommand: commandName: " + commandName);
                    switch (commandName){
                        case "openJava":
                            Intent intent=new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://www.javatpoint.com"));
                            startActivity(intent);
                            break;
                        case "openContact":
                            String myData = "content://contacts/people/";
                            Intent myActivity2 = new Intent(Intent.ACTION_VIEW, Uri.parse( myData) );
                            startActivity(myActivity2);
                            break;
                        case "openTab":
                            int tab = Integer.parseInt(eventCommand.getData().getJSONObject("data").getString("tab"));
                            tabs.getTabAt(tab);
                            break;
                        default:
                            // code block
                    }
                } catch (JSONException e) {
                    Log.e("AlanButton", e.getMessage());
                }
            }
        };

        /// Registering callbacks
        alanButton.registerCallback(alanCallback);
    }
}