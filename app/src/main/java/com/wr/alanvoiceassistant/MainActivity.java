package com.wr.alanvoiceassistant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.alan.alansdk.AlanCallback;
import com.alan.alansdk.AlanConfig;
import com.alan.alansdk.button.AlanButton;
import com.alan.alansdk.events.EventCommand;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    AlanButton alanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /// Defining the project key
        AlanConfig config = AlanConfig.builder().setProjectId("b5fd04cf79f3b2f6c443512913fe289a2e956eca572e1d8b807a3e2338fdd0dc/stage").build();
        alanButton = findViewById(R.id.alan_button);
        alanButton.initWithConfig(config);
        AlanCallback alanCallback = new AlanCallback() {
            /// Handling commands from Alan Studio
            @Override
            public void onCommand(final EventCommand eventCommand) {
                try {
                    JSONObject command = eventCommand.getData();
                    String commandName = command.getJSONObject("data").getString("command");
                    Log.d("AlanButton", "onCommand: commandName: " + commandName);
                    switch (commandName){
                        case "openJava":
                            Intent intent=new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://www.javatpoint.com"));
                            startActivity(intent);
                            break;
                        case "openContact":
                            String myData = "content://contacts/people/";
                            Intent myActivity2 = new Intent(Intent.ACTION_VIEW, Uri.parse( myData) );
                            startActivity(myActivity2);
                            break;
                        default:
                            // code block
                    }
                } catch (JSONException e) {
                    Log.e("AlanButton", e.getMessage());
                }
            }
        };

        /// Registering callbacks
        alanButton.registerCallback(alanCallback);
    }
}